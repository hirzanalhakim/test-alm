import React from "react";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import ShoppingCartOutlined from "@material-ui/icons/ShoppingCartOutlined";
import KeyboardArrowDownOutlinedIcon from "@material-ui/icons/KeyboardArrowDownOutlined";

import Tabs from "./Tabs";
import Tab from "./Tab";
import PortfolioSubMenu from "./PortfolioSubMenu";



const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  search: {
    height: 30,
    position: "relative",
    border: "2px solid #999999",
    borderRadius: 8,
    marginLeft: 16,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "auto",
    },
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: theme.spacing(1),
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
      alignItems: "center",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  labelText: { display: "flex", alignItems: "center" },
}));

export default function PrimarySearchAppBar(props) {
  const { value, handleChange } = props
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [isOpen, setIsOpen] = React.useState(false);

  const isMenuOpen = Boolean(anchorEl);

  const handleMobileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleSubMenuPortfolioClose = () => {
    setIsOpen(false);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleSubMenuPortfolioClose();
  };

  const handleSubMenuPortfolioOpen = (event) => {
    setIsOpen(true);
  };

  const renderMobileMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem
        onClick={(e) => {
          handleChange(e, 0);
          handleMenuClose();
        }}
      >
        Demo
      </MenuItem>
      <MenuItem disabled>Pages</MenuItem>
      <MenuItem
        onClick={(e) => {
          handleChange(e, 2);
          handleMenuClose();
        }}
      >
        Portfolio
      </MenuItem>
    </Menu>
  );

  return (
    <div>
      <div className={classes.grow}>
        <AppBar position="static" color="transparent">
          <Toolbar>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="simple tabs example"
            >
              <Tab
                label={
                  <div className={classes.labelText}>
                    Demo <KeyboardArrowDownOutlinedIcon />
                  </div>
                }
              />
              <Tab
                disabled
                label={
                  <div className={classes.labelText}>
                    Pages <KeyboardArrowDownOutlinedIcon />
                  </div>
                }
              />
              <Tab
                label={
                  <ClickAwayListener onClickAway={handleSubMenuPortfolioClose}>
                    <Typography
                      onMouseEnter={handleSubMenuPortfolioOpen}
                      className={classes.labelText}
                    >
                      Portfolio <KeyboardArrowDownOutlinedIcon />
                    </Typography>
                  </ClickAwayListener>
                }
              />
            </Tabs>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              <IconButton edge="end">
                <ShoppingCartOutlined />
              </IconButton>

              <IconButton edge="end">
                <SearchIcon />
              </IconButton>
              <div className={classes.search}>
                <InputBase
                  classes={{
                    input: classes.inputInput,
                  }}
                />
              </div>
            </div>
            <div className={classes.sectionMobile}>
              <IconButton edge="end">
                <SearchIcon />
              </IconButton>
              <IconButton edge="end">
                <ShoppingCartOutlined />
              </IconButton>
              <IconButton edge="end" onClick={handleMobileMenuOpen}>
                <MenuIcon />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
      </div>
      {renderMobileMenu}
      {isOpen && <PortfolioSubMenu />}
    </div>
  );
}
