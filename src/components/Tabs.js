import React from "react";
import Tabs from "@material-ui/core/Tabs";
import { withStyles } from "@material-ui/core/styles";


const StyledTabs = withStyles((theme) => ({
  root: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  indicator: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "transparent",
    "& > span": {
      display: "none",
      width: "100%",
      backgroundColor: "#635ee7",
    },
  },
}))((props) => <Tabs {...props} TabIndicatorProps={{ children: <span /> }} />);

export default StyledTabs;
