import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  container: {
    position: "relative",
    width: "100%",
  },
  wrapper: {
    position: "absolute",
    background: "white",
    width: "100%",
  },
  contentWrapper: {
    padding: "32px",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  titleText: { color: "#2e81ff", fontWeight: 600, marginBottom: 16 },
  submenuText: { color: "#1f2371", fontWeight: 600 },
}));

const PortfolioSubMenu = () => {
  const classes = useStyles();

  const menuRows = (
    <>
      <Typography gutterBottom className={classes.submenuText}>
        2 columns
      </Typography>
      <Typography gutterBottom className={classes.submenuText}>
        3 columns
      </Typography>
      <Typography gutterBottom className={classes.submenuText}>
        4 columns
      </Typography>
      <Typography gutterBottom className={classes.submenuText}>
        Full width
      </Typography>
    </>
  );

  return (
    <div className={classes.container}>
      <div className={classes.wrapper}>
        <Grid
          container
          spacing={3}
          direction="row"
          className={classes.contentWrapper}
        >
          <Grid item md={4}>
            <Typography gutterBottom className={classes.titleText}>
              Grid Minimal
            </Typography>
            {menuRows}
          </Grid>

          <Grid item md={4}>
            <Typography gutterBottom className={classes.titleText}>
              Masonry Minimal
            </Typography>
            {menuRows}
          </Grid>

          <Grid item md={4}>
            <Typography gutterBottom className={classes.titleText}>
              Grid Detail
            </Typography>
            {menuRows}
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default PortfolioSubMenu;
