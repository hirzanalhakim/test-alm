import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import KeyboardArrowRightOutlinedIcon from "@material-ui/icons/KeyboardArrowRightOutlined";
import MenuItem from "@material-ui/core/MenuItem";
import ShoppingCartOutlined from "@material-ui/icons/ShoppingCartOutlined";
import ChatBubbleOutlineIcon from "@material-ui/icons/ChatBubbleOutline";
import DonutLargeOutlined from "@material-ui/icons/DonutLargeOutlined";
import DescriptionOutlinedIcon from "@material-ui/icons/DescriptionOutlined";
import ArrowRight from "@material-ui/icons/ArrowRight";
import VideocamOutlinedIcon from "@material-ui/icons/VideocamOutlined";
import SupervisorAccountOutlinedIcon from "@material-ui/icons/SupervisorAccountOutlined";

import "./Demos.css";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    display: "flex",
    minHeight: "100vh",
    backgroundImage: `url('https://cdn.pixabay.com/photo/2017/02/15/10/39/food-2068217_1280.jpg')`,
    backgroundPosition: "center",
    backgroundSize: "cover",
  },
  floatNavWrapper: {
    textAlign: "center",
    flex: 1,
    color: "#fff",
  },
  floatingNav: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
      flexDirection: "row",
      position: "fixed",
      float: "right",
      right: 0,
      top: "30%",
    },
  },
  menuIcon: { fontSize: 18, color: "#999999" },
  menuItemWrapper: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  menuItem: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    marginRight: 8,
  },
  popDesc: {
    padding: 12,
    borderRadius: 50,
    backgroundColor: "#FFFFFF",
  },
  descText: { color: "green" },
  arrowIcon: {
    marginLeft: -10,
  },
  textWrapper: { minHeight: "70vh", padding: "48px 16px 48px 16px" },
  btnQuote: {
    marginTop: 48,
    borderRadius: 50,
    textTransform: "none",
    color: "#FFF",
    backgroundColor: "#4CAF50",
    alignItems: "center",
  },
}));

export default function Demos() {
  const classes = useStyles();
  const [isMenuDescOpen, setIsMenuDescOpen] = React.useState(false);

  const floatMenus = [
    {
      icon: <ChatBubbleOutlineIcon className={classes.menuIcon} />,
      desc: "Presale Chat",
    },
    {
      icon: <DonutLargeOutlined className={classes.menuIcon} />,
      desc: "Submit Support Ticket",
    },
    {
      icon: <DescriptionOutlinedIcon className={classes.menuIcon} />,
      desc: "Read Documentation",
    },
    {
      icon: <VideocamOutlinedIcon className={classes.menuIcon} />,
      desc: "See Videos",
    },
    {
      icon: <SupervisorAccountOutlinedIcon className={classes.menuIcon} />,
      desc: "Contact Us",
    },
    {
      icon: <ShoppingCartOutlined className={classes.menuIcon} />,
      desc: "Cart",
    },
  ];

  return (
    <div className={classes.wrapper}>
      <div className={classes.floatNavWrapper}>
        <div className={classes.floatingNav}>
          <div>
            {floatMenus.map((item, index) => {
              return (
                <div className={classes.menuItemWrapper}>
                  {isMenuDescOpen !== index && <div />}
                  {isMenuDescOpen === index && (
                    <div className={classes.menuItem}>
                      <div className={classes.popDesc}>
                        <Typography
                          variant="body2"
                          className={classes.descText}
                        >
                          <b>{item.desc}</b>
                        </Typography>
                      </div>
                      <ArrowRight className={classes.arrowIcon} />
                    </div>
                  )}
                  <MenuItem
                    style={{
                      background: "#FFF",
                      height: 45,
                      width: 45,
                      borderTopLeftRadius: index === 0 ? 8 : 0,
                      borderTopRightRadius: index === 0 ? 8 : 0,
                      borderBottomLeftRadius:
                        index === floatMenus.length - 1 ? 8 : 0,
                      borderBottomRightRadius:
                        index === floatMenus.length - 1 ? 8 : 0,
                      borderBottom: "1px solid #ddd",
                    }}
                    onMouseEnter={() => setIsMenuDescOpen(index)}
                    onMouseLeave={() => setIsMenuDescOpen(-1)}
                  >
                    {item.icon}
                  </MenuItem>
                </div>
              );
            })}
          </div>
        </div>
        <div className={classes.textWrapper}>
          <Typography
            gutterBottom
            variant="h4"
            style={{ fontFamily: "cursive" }}
          >
            Catering should be an experience
          </Typography>
          <Typography gutterBottom variant="h2">
            <b>
              We use only the finest and <br /> the freshest ingredients
            </b>
          </Typography>
          <Typography gutterBottom variant="h6">
            At Sway catering we know that food is an important part of life{" "}
            <br />
            if the meal is not perfect, your event can not be perfect.
          </Typography>
          <Button variant="contained" className={classes.btnQuote}>
            Request a Quote
            <KeyboardArrowRightOutlinedIcon fontSize="small" />
          </Button>
        </div>

        <div style={{ position: "relative" }}>
          <div class="custom-shape-divider-bottom-opacity">
            <svg
              data-name="Layer 1"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 1200 120"
              preserveAspectRatio="none"
            >
              <path
                d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z"
                class="shape-fill-opacity"
              ></path>
            </svg>
          </div>

          <div class="custom-shape-divider-bottom-1618210139">
            <svg
              data-name="Layer 1"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 1200 120"
              preserveAspectRatio="none"
            >
              <path
                d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z"
                class="shape-fill"
              ></path>
            </svg>
          </div>
        </div>

        <div
          style={{
            padding: "48px 16px 48px 16px",
            backgroundColor: "#FFF",
          }}
        >
          <Typography
            gutterBottom
            variant="h4"
            style={{ fontFamily: "cursive", color: "#999999" }}
          >
            Catering service in New York
          </Typography>
          <Typography gutterBottom variant="h4" style={{ color: "#1f2371" }}>
            <b>We specialize in corporate and private events</b>
          </Typography>
          <Typography
            gutterBottom
            variant="h6"
            style={{ color: "#4d4d4d", fontSize: "1rem" }}
          >
            With 20 years experiences, we promise you the freshest, local
            ingredients, hand-crafted cooking sprinkle with our unique
            whimsicaleleganceand execptional service
          </Typography>
        </div>
      </div>
    </div>
  );
}
