import React from "react";
import Portfolio from '../Portfolio/Portfolio';
import Demos from "../Demos/Demos";
import TabPanel from "../../components/TabPanel";
import NavBar from "../../components/NavBar";

export default function PrimarySearchAppBar() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <NavBar handleChange={handleChange} value={value} />

      <div>
        <TabPanel value={value} index={0}>
          <Demos />
        </TabPanel>
        <TabPanel value={value} index={1}>
          Pages
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Portfolio />
        </TabPanel>
      </div>
    </div>
  );
}
