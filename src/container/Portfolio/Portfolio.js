import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import FilterNoneOutlined from "@material-ui/icons/FilterNoneOutlined";
import EcoOutlined from "@material-ui/icons/EcoOutlined";
import ThumbUpOutlined from "@material-ui/icons/ThumbUpOutlined";
import AllInclusiveOutlined from "@material-ui/icons/AllInclusiveOutlined";

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: "#322629",
    flex: 1,
    paddingBottom: 70,
  },
  portfolioSection: { background: "#404040", marginBottom: 16 },
  portfolioText: {
    color: "#FFF",
    fontWeight: 600,
    minHeight: 80,
    paddingTop: 16,
  },
  jobSection: { padding: "0px 32px 32px 32px" },
  jobWrapper: { padding: "0px 16px 16px 16px" },
  jobIcon: {
    color: "#A2868F",
    marginBottom: 24,
    fontSize: "3rem",
  },
  jobInfoWrapper: { background: "#38292e", padding: 24 },
  jobTitleText: { color: "#FF7280", fontWeight: 600 },
  jobDescText: { color: "#A2868F" },
  serviceWrapper: { padding: 16 },
  serviceTitle: {
    color: "#FF7280",
    fontWeight: 600,
    marginBottom: 32,
  },
  serviceText: { color: "#A2868F", marginBottom: 16 },
  btnDownload: {
    borderRadius: 50,
    textTransform: "none",
    color: "#FFF",
    fontSize: 12,
    borderColor: "#FF7280",
    backgroundColor: "#322629",
    alignItems: "center",
    marginRight: 16,
    marginTop: 16,
  },
  btnCheck: {
    marginTop: 16,
    borderRadius: 50,
    textTransform: "none",
    color: "#FFF",
    fontSize: 12,
    borderColor: "#FF7280",
    backgroundColor: "#322629",
    alignItems: "center",
  },
  skillWrapper: { marginTop: 24 },
  skillText: {
    color: "#FF7280",
    fontWeight: 600,
    marginBottom: 32,
    marginLeft: 16,
  },
  skillRangeWrapper: { marginBottom: 48 },
  circleRangeWrapper: {
    position: "relative",
    display: "inline-block",
    width: 200,
    marginTop: 30,
    transform: "scaleY(-1)",
  },
  svgCircle: { transform: "rotate(90deg)" },
  circleData: {
    zIndex: 0,
    position: "absolute",
    top: 0,
    boxShadow: "5px 5px 5px 5px #333",
    stroke: "#584147",
  },
  circleRangeText: {
    width: "60%",
    textAlign: "center",
    position: "absolute",
    top: 85,
    left: "20%",
    fontSize: 32,
    fontWeight: 100,
    color: "#FF7280",
    transform: "scaleY(-1)",
  },
  circleTitleText: {
    color: "#FF7280",
    marginTop: 16,
    marginLeft: 16,
  },
  barSection: { width: "60%", padding: 16 },
  barWrapper: {
    alignItems: "center",
    flexDirection: "row",
    width: "100%",
  },
  barRange: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
  },
  barTitle: {
    color: "#A2868F",
    marginTop: 16,
  },
  barRangeText: {
    color: "#584147",
    marginLeft: 16,
  },
}));

const Portfolio = () => {
  const classes = useStyles();

  const circleSkills = [
    { title: "Photography", range: 57 },
    { title: "Marketing", range: 60 },
    { title: "PhP", range: 74 },
    { title: "3D", range: 31 },
  ];

  const barSkills = [
    { title: "Creativity", range: 75 },
    { title: "PhP", range: 90 },
    { title: "Cooking", range: 48 },
    { title: "Marketing", range: 62 },
  ];

  const jobs = [
    {
      icon: <FilterNoneOutlined className={classes.jobIcon} />,
      title: "Design",
      desc:
        "a full stack all around designer that may or may not include a guide for specific reative people",
    },
    {
      icon: <AllInclusiveOutlined className={classes.jobIcon} />,
      title: "Develop",
      desc:
        "a full stack all around designer that may or may not include a guide for specific reative people",
    },
    {
      icon: <EcoOutlined className={classes.jobIcon} />,
      title: "Write",
      desc:
        "a full stack all around designer that may or may not include a guide for specific reative people",
    },
    {
      icon: <ThumbUpOutlined className={classes.jobIcon} />,
      title: "Promote",
      desc:
        "a full stack all around designer that may or may not include a guide for specific reative people",
    },
  ];

  return (
    <div>
      <Grid container className={classes.wrapper}>
        <Grid item container direction="row">
          <Grid
            md={2}
            item
            container
            alignItems="center"
            justify="center"
            className={classes.portfolioSection}
          >
            <Typography variant="h6" className={classes.portfolioText}>
              <i>My Portfolio</i>
            </Typography>
          </Grid>
          <Grid md={6} item container className={classes.jobSection}>
            {jobs.map((data, index) => {
              return (
                <Grid key={index} md={6} item className={classes.jobWrapper}>
                  <div className={classes.jobInfoWrapper}>
                    {data.icon}
                    <Typography variant="h6" className={classes.jobTitleText}>
                      {data.title}
                    </Typography>
                    <Typography variant="body2" className={classes.jobDescText}>
                      {data.desc}
                    </Typography>
                  </div>
                </Grid>
              );
            })}
          </Grid>
          <Grid md={4} item>
            <div className={classes.serviceWrapper}>
              <Typography variant="h6" className={classes.serviceTitle}>
                Service
              </Typography>
              <Typography variant="body1" className={classes.serviceText}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam.
              </Typography>

              <div className={classes.serviceText}>
                <Typography variant="caption">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat.
                </Typography>
              </div>
              <Typography variant="body2" className={classes.serviceText}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </Typography>

              <div>
                <Button variant="outlined" className={classes.btnDownload}>
                  Download CV
                </Button>
                <Button variant="outlined" className={classes.btnCheck}>
                  Check My Portfolio
                </Button>
              </div>
            </div>
          </Grid>
        </Grid>

        {/* SKILLS */}
        <Grid item container className={classes.skillWrapper}>
          <Grid item>
            <Typography variant="h5" className={classes.skillText}>
              Skills
            </Typography>
          </Grid>

          <Grid
            container
            direction="row"
            justify="space-around"
            className={classes.skillRangeWrapper}
          >
            {circleSkills.map((data) => {
              return (
                <Grid
                  item
                  md={3}
                  container
                  direction="column"
                  alignItems="center"
                  justify="center"
                >
                  <div className={classes.circleRangeWrapper}>
                    <svg height="200" width="200" className={classes.svgCircle}>
                      <circle
                        className={classes.circleData}
                        cx="100"
                        cy="100"
                        r="80"
                        strokeWidth="18"
                        fill="transparent"
                        strokeDasharray="753"
                      />
                      <circle
                        style={{
                          strokeDashoffset:
                            data.range > 50
                              ? 500 - data.range
                              : 630 - data.range,
                          stroke: "#FF7280",
                          transition: "stroke 1s linear",
                          zIndex: 1,
                          position: "absolute",
                          top: 0,
                        }}
                        cx="100"
                        cy="100"
                        r="80"
                        stroke-width="35"
                        fill="transparent"
                        stroke-dasharray="753"
                      />
                    </svg>
                    <div className={classes.circleRangeText}>
                      <b>{`${data.range}%`}</b>
                    </div>
                  </div>
                  <div>
                    <Typography
                      variant="h6"
                      className={classes.circleTitleText}
                    >
                      {data.title}
                    </Typography>
                  </div>
                </Grid>
              );
            })}
          </Grid>

          <Grid container direction="row" justify="space-around">
            {barSkills.map((data) => {
              return (
                <Grid item md={6} container direction="row" justify="center">
                  <div className={classes.barSection}>
                    <div className={classes.barWrapper}>
                      <div className={classes.barRange}>
                        <div
                          style={{
                            textAlign: "center",
                            width: `${data.range}%`,
                            height: 20,
                            backgroundColor: "#FF7280",
                          }}
                        />

                        <div
                          className={{
                            textAlign: "center",
                            width: `${100 - data.range}%`,
                            height: 10,
                            backgroundColor: "#584147",
                          }}
                        />
                      </div>
                    </div>
                    <Typography variant="h6" className={classes.barTitle}>
                      {data.title}
                    </Typography>
                  </div>
                  <div>
                    <Typography variant="h4" className={classes.barRangeText}>
                      <b>{`${data.range}%`}</b>
                    </Typography>
                  </div>
                </Grid>
              );
            })}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default Portfolio;
