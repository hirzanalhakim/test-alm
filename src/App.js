
import StaticPage from './container/StaticPage/StaticPage'

function App() {
  return (
    <>
      <StaticPage />
    </>
  );
}

export default App;
