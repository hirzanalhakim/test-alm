# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How to Run this Project

In the project directory, you can run:

1. ### `yarn install`

this script is for installing the whole package

2. ### `yarn start`

this script is for starting the app in the development mode
